const fromPx = (value) => {
	if (!value) return 0;
	return parseFloat(value.toString().replace('px', '').trim());
}

const fromTranslate = (value) => {
	return value.replace(/[a-z()]/g, '').split(',').map((position) => parseInt(position));
}

const getTextBBox = (node, index) => d3.select(node).select('text:nth-of-type(' + (index) + ')').node().getBBox();
const getElementByIndex = (node, index) => d3.select(node).select('rect:nth-of-type(' + (index) + ')');
const getDataIndex = (node) => parseInt(d3.select(node).attr('data-index'));
const calculateElementX = (parentNode, i) => {
	let previousXEnd = 0;
	if (i > 0) {
		const previousRect = getElementByIndex(parentNode, i);
		previousXEnd = fromPx(previousRect.attr('x'));
		previousXEnd += fromPx(previousRect.style('width'));
	}
	return previousXEnd;
};

function getClipBox () {
	return 'url(#clip-box-' + d3.select(this.parentNode).attr('data-index') + ')';
}

const getDragReferences = (group) => {
	const me = d3.select(group);
	const parent = d3.select(group.parentNode);
	const clipPathRect = parent.select('#clip-box-' + me.attr('data-index')).select('rect');
	const myBBox = group.getBBox();
	const parentRect = parent.select('rect:nth-of-type(1)');
	const parentWidth = fromPx(parentRect.attr('width'));

	return {
		me,
		clipPathRect,
		myBBox,
		parentRect,
		parentWidth
	};
}

function hierarchyLevelChart (data, {
	levelHeight = 40,
	levelSpacing = 5,
	textMargin = 10,
	elementHeight = 30,
	elementMargin = 5,
	groupsColor = '#424242',
	appendTo = 'body',
	svgWidth = '100%',
	svgHeigh = 900,
	levelTextColor = '#424242',
	elementTextColor = '#f5f5f5',
	elementsColor = '#616161'
} = {}) {

	const svgContainer = d3
		.select(appendTo)
		.append('svg')
		.attr('width', svgWidth)
		.attr('height', svgHeigh);

	const viewWidth = svgContainer
		.node()
		.getBoundingClientRect()
		.width;

	const levels = svgContainer.selectAll(null)
		.data(data)
		.enter();

	const levelsText = levels.append('text')
		.attr('fill', levelTextColor)
		.classed('hlc-level-text', true);
	levelsText.append('tspan')
		.text((d) => d.name)
		.attr('text-anchor', 'middle')
		.attr('x', 0);
	levelsText.append('tspan')
		.text((d) => d.level)
		.attr('text-anchor', 'middle')
		.attr('x', 0)
		.attr('dy', '1rem');

	let maxLevelTextWidth = 0;
	levelsText.each(function () {
		maxLevelTextWidth = Math.max(maxLevelTextWidth, this.getBBox().width);
	});
	maxLevelTextWidth += textMargin;

	levelsText
		.attr('transform', 'translate(' + (maxLevelTextWidth / 2) + ')')
		.attr('y', function (d, i) {
			const textHeight = this.getBBox().height;
			return ((i + 1) * (levelHeight + levelSpacing)) - textHeight;
		});

	const group = levels
		.append('g')
		.attr('transform', 'translate(' + maxLevelTextWidth + ')');
	
	group
		.append('defs')
		.append('clipPath')
		.attr('id', (d, i) => 'clip-box-' + i)
		.append('rect')
		.attr('width', viewWidth - maxLevelTextWidth)
		.attr('height', levelHeight);

	group
		.append('rect')
		.classed('hlc-group-rect', true)
		.attr('x', '0')
		.attr('y', (d, i) => (i * (levelHeight + levelSpacing)))
		.attr('fill', groupsColor)
		.attr('width', viewWidth - maxLevelTextWidth)
		.attr('height', levelHeight);

	const elementsGroup = group
		.append('g')
		.style('clip-path', (d, i) => 'url(#clip-box-' + i + ')')
		.attr('data-index', (d, i) => i)
		.attr('transform', (d, i) => 'translate(0,' + (i * (levelHeight + levelSpacing)) + ')')
		.call(
			d3.drag()
				.on('drag', function (d, i) {
					const {
						me,
						clipPathRect,
						myBBox,
						parentWidth
					} = getDragReferences(this);
					if (myBBox.width < parentWidth)
						return void 0;
					const [previousX, previousY] = fromTranslate(me.attr('transform'));
					me.attr('transform', 'translate(' +
						(isNaN(previousX) ? d3.event.dx : previousX + d3.event.dx)
						+ ',' + previousY + ')'
					);
					const previousXClipPath = fromPx(clipPathRect.attr('x'));
					clipPathRect.attr('x', (previousXClipPath - d3.event.dx));
				})
				.on('end', function () {
					const {
						me,
						clipPathRect,
						myBBox,
						parentWidth
					} = getDragReferences(this);
					if (myBBox.width < parentWidth)
						return void 0;
					const [currentX, currentY] = fromTranslate(me.attr('transform'));
					const minX = parentWidth - myBBox.width - (2 * elementMargin);

					if (currentX < minX) {
						me.transition()
							.duration(200)
							.attr('transform', 'translate(' + minX + ',' + currentY + ')')
						clipPathRect.transition()
							.duration(200)
							.attr('x', minX * -1);
					}
					else if (currentX > 0) {
						me.transition()
							.duration(200)
							.attr('transform', 'translate(0,' + currentY + ')')
						clipPathRect.transition()
							.duration(200)
							.attr('x', 0);
					}

				})
		)
		.selectAll(null)
		.data(({ elements }) => elements)
		.enter();

	const elements = elementsGroup.append('rect');

	const texts = elementsGroup
		.append('text')
		.classed('hlc-element-text', true)
		.text(({ name }) => name)
		.style('cursor', 'move')
		.attr('fill', elementTextColor)
		.attr('y', function () {
			const textHeight = this.getBBox().height;
			return (elementHeight / 2 + elementMargin + textHeight / 4);
		});
	
	elements
		.classed('hlc-element-rect', true)
		.attr('fill', elementsColor)
		.attr('y', elementMargin)
		.attr('width', function (d, i) {
			return getTextBBox(this.parentNode, i + 1).width + (textMargin * 2);
		})
		.attr('height', elementHeight)
		.attr('x', function (d, i) {
			const previousXEnd = calculateElementX(this.parentNode, i);
			return (previousXEnd + elementMargin);
		});

	texts
		.attr('x', function (d, i) {
			const previousXEnd = calculateElementX(this.parentNode, i);
			return (previousXEnd + elementMargin + textMargin);
		});

	return svgContainer;

}